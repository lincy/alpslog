#!/usr/bin/python
import time
import MySQLdb

import numpy as np
from matplotlib import pyplot as plt

dbcon = MySQLdb.connect(host='localhost', user='alps', passwd='alps', db='alps')
db = dbcon.cursor()

###
#db.execute("""select COUNT(*) from acct""" )
#print "Total job : %d " % db.fetchone()
#db.execute("""select COUNT(*) from acct where gotime = 0""" )
#print "Job not run : %d " % db.fetchone()

##
queue=dict()
Nnrun=dict()
Tnrun=dict()
Unrun=dict()
Bnrun=dict()
Nkbrun=dict()
Tkbrun=dict()
Ukbrun=dict()
Bkbrun=dict()
Nr=dict()
Twait=dict()
Uwait=dict()
Bwait=dict()
Trun=dict()
Urun=dict()
Brun=dict()


###
db.execute("""select queue, count(queue), sum(gotime-subtime),max(gotime-subtime),min(gotime-subtime), sum(logtime-gotime),max(logtime-gotime),min(logtime-gotime) from acct WHERE (gotime>0) GROUP BY queue;""" )
avg_wtime_run = db.fetchall()
for row in avg_wtime_run:
  Nr[row[0]] = row[1]
  Twait[row[0]] = float(row[2])/60/row[1]
  Uwait[row[0]] = row[3]/60
  Bwait[row[0]] = row[4]/60
  Trun[row[0]] = float(row[5])/60/row[1]
  Urun[row[0]] = row[6]/60.
  Brun[row[0]] = row[7]/60.

xp = np.arange(len(Nr))

plt.xticks(xp, Nr.keys(),rotation=90)
plt.plot(xp, Twait.values(), label="Avg waiting")
plt.plot(xp, Uwait.values(), label="Max waiting")
plt.plot(xp, Bwait.values(), label="Min waiting")
plt.xlabel('Queue')
plt.legend(loc='upper right')
#plt.yscale('log', nonposy='clip')
plt.show()

xp = np.arange(len(Nr))
plt.xticks(xp, Nr.keys(),rotation=90)
plt.plot(xp, Trun.values(), label="Avg run")
plt.plot(xp, Urun.values(), label="Max run")
plt.plot(xp, Brun.values(), label="Min run")
plt.xlabel('Queue')
plt.legend(loc='upper right')
plt.show()

###
db.execute("""select queue, count(queue), sum(logtime-subtime),max(logtime-subtime),min(logtime-subtime) from acct WHERE (gotime=0) GROUP BY queue ORDER by queue;""" )
avg_wtime = db.fetchall()
for row in avg_wtime:
  Nnrun[row[0]] = row[1]
  Tnrun[row[0]] = float(row[2])/row[1]/60
  Unrun[row[0]] = row[3]/60
  Bnrun[row[0]] = row[4]/60


xp = np.arange(len(Nnrun))
plt.xticks(xp, Nnrun.keys(),rotation=90)
plt.plot(xp, Tnrun.values(), label="Avg norun")
plt.plot(xp, Unrun.values(), label="Max norun")
plt.plot(xp, Bnrun.values(), label="Min norun")
plt.xlabel('Queue')
plt.legend(loc='upper right')
plt.show()

###
db.execute("""select queue, count(queue), sum(logtime-subtime),max(logtime-subtime),min(logtime-subtime) from acct WHERE ((exitInfo=14 or exitInfo=8 or exitInfo=10) and gotime=0) GROUP BY queue ORDER BY queue;""" )
avg_wtime_user_kill = db.fetchall()
for row in avg_wtime_user_kill:
  Nkbrun[row[0]] = row[1]
  Tkbrun[row[0]] = row[2]/row[1]/60
  Ukbrun[row[0]] = row[3]/60
  Bkbrun[row[0]] = row[4]/60

xp = np.arange(len(Nkbrun))
plt.xticks(xp, Nkbrun.keys(),rotation=90)
plt.plot(xp, Tkbrun.values(), label="Avg kbrun")
plt.plot(xp, Ukbrun.values(), label="Max kbrun")
plt.plot(xp, Bkbrun.values(), label="Min kbrun")
plt.xlabel('Queue')
plt.legend(loc='upper right')
plt.show()

#print "#### Job killed by users"
#print "## queue, count, avg waiting time(min)"
#for row in db.fetchall():
#  print row[0], row[1], row[2]


###
#  print row[0], row[1], row[2]





#db.execute("""select queue, count(queue) from acct GROUP BY queue;""" )
#row = db.fetchone()
#while row is not None:
#    print row[0], row[1]
#    row = db.fetchone()
        