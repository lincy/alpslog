#include <cfloat>
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
using std::cout;
using std::cerr;
using std::endl;
using std::ifstream;
#include <algorithm> // std::nth_element

int main (int argc, char* argv[]) {

    if (argc!=3) {
	cerr << "Usage: gephi_csv <in> <out>" << endl;
	return -1;
    }
    
    const int BUF=256;
    double v[3];

    char *fname = argv[1];
    char *oname = argv[2];
    char comment[BUF];
    ifstream file(fname,std::ios::in) ;

    std::ofstream output;
    output.open(oname, std::ios::out);

    output << "Source,Target,Weight" << endl;
    //
    // load all vectors into v
    //
    int count=0;
    for (std::string line; getline(file,line); ) {
	std::stringstream ss(line);
	ss >> v[0]>> v[1]>> v[2];
	if (!ss) {
	  continue;
	  //cout << line << endl;
	}
	
	output << v[0] << ","  << v[1] << ","  << v[2] << endl;
	count++;
    }
    file.close();
    output.close();

    //
    cout << "## Proceed lines : " << count << endl;

}

