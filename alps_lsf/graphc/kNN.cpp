#include <boost/numeric/ublas/matrix_sparse.hpp>
#include <boost/numeric/ublas/io.hpp>

#include <cstdio>
#include <cfloat>
#include <cmath>
#include <iostream>
#include <fstream>
using std::cout;
using std::cerr;
using std::endl;
using std::ifstream;
#include <algorithm> // std::nth_element

	typedef boost::numeric::ublas::compressed_matrix<float>::iterator1 it1_t;
	typedef boost::numeric::ublas::compressed_matrix<float>::iterator2 it2_t;
	
typedef struct IDL_ {
    int id;
    float dist;
} IDL;

bool IDLcmp( IDL i,  IDL j) { return (i.dist<j.dist); }
int main () {

    const int BUF=1024;
    const int N=50000, M=18, K=5;    // RAM: N*K*M * 8 Byte
    //const int N=10, M=18, K=4;    // RAM: N*K*M * 8 Byte
    char fname[] = "log_vecM.txt";
    char oname[20];
    sprintf(oname, "v%dk%d.txt", N,K);
    

    cout << "Input  filename : " << fname << endl;
    cout << "Output filename : " << oname << endl;
    printf("N=%d, M=%d, K=%d, Mem=%f MB\n", N,M,K,N*M*8./1024./1024.);
    
    double v[M*N];
    double max[M];
    
    char comment[BUF];
    ifstream file(fname);
    if (!file) {
	cerr << "Read file fail ! -- " << fname << endl;
	return -1;
    }
    
    //
    // load all vectors into v
    //
    
    // skip the first ## line
    file.getline(comment, BUF);   //cout << comment <<endl;
    // read the first data row
    cout << "Max:: "<<endl;
    for (int i = 0; i < M; i++) {
	file >> max[i];
        cout << max[i] << " ";
        max[i] = 1.0/(max[i]);
    }
    cout << "---"<<endl;

    // read the rest data
    int count=0;
    while (!file.eof()) {
	if (count>=N) break;

	for (int i = 0; i < M; i++) file >> v[count*M+i];
        file.getline(comment, BUF);
	//for (int i = 0; i < M; i++) cout << v[count*M+i] << " ";
        //cout << endl;
	count++;
    }
    file.close();
    printf("Output %d records.\n", count);


    //
    //  generate K-nearest matrix
    //
    using namespace boost::numeric::ublas;
    compressed_matrix<float> D(N, N, N * K);

    std::vector<IDL> dl(N);
    for (int i = 0; i < N; i++) {
    
	// all distance from i-node
        for (int j = 0; j < N; j++) {
	    dl[j].id  =j;
	    double sum=0, tmp;
            for (int p = 2; p < M; p++) {  // the first 2 value is not counting
        	tmp = (v[i*M+p]-v[j*M+p])*max[p];
        	sum +=  tmp*tmp;
	    }
    	    dl[j].dist = sqrt(sum);
	}
	dl[i].dist = DBL_MAX;

	// find the K-smallest dl
	std::nth_element(dl.begin(), dl.begin()+K, dl.end(), IDLcmp );
        #ifdef DEBUG
        for (int p = 0; p < K; p++) cout << dl[p].id << " " << dl[p].dist << " ";
	getchar();
	#endif

	// store in sparse matrix A
        for (int p=0; p < K; p++) {
            if (i<dl[p].id) D(i, dl[p].id) = dl[p].dist;
	    else            D(dl[p].id, i) = dl[p].dist;
	    
	    //cout << i << " " << dl[p].id << " "<< dl[p].dist << endl;
	}
    
    } // end for i

    
    
    // write sparse matrix
    std::ofstream output;
    output.open(oname, std::ios::out);
    // list all non-zero
    for (it1_t it1 = D.begin1(); it1 != D.end1(); it1++)
    for (it2_t it2 = it1.begin(); it2 != it1.end(); it2++) {
        output << it2.index1() << " " << it2.index2() << "  " << *it2 << endl;
    }
    output.close();

}

