#!/usr/bin/python
import time
import MySQLdb

import numpy as np
from matplotlib import pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
pp = PdfPages('logs.pdf')
import pylab as pl

dbcon = MySQLdb.connect(host='localhost', user='alps', passwd='alps', db='alps')
db = dbcon.cursor()

###
print "=== Mem =====  "
db.execute("""SELECT maxRMem,maxRSwap FROM `acct` where shtext>0""")
rows= db.fetchall()
y1=[]
y2=[]
for row in rows:
  y1.append(row[0])
  y2.append(row[1])
pl.hist(y1, bins=np.logspace(0, np.log10(max(y1)), 300 ), alpha=.5,label='maxRMem' )
pl.hist(y2, bins=np.logspace(0, np.log10(max(y1)), 300 ), alpha=.5,label='maxRSwap' )
pl.gca().set_xscale("log")
pl.xlabel('Size')
pl.ylabel('Numbers')
pl.legend(loc='upper right')
#pl.show()
pp.savefig()
pl.clf()

###
print "=== CS =====  "
db.execute("""SELECT nvcsw, nivcsw FROM `acct` where shtext>0""")
rows= db.fetchall()
y1=[]
y2=[]
for row in rows:
  y1.append(row[0])
  y2.append(row[1])

pl.hist(y1, bins=np.logspace(0, np.log10(max(y1)), 100 ), alpha=.5, label='V' )
pl.hist(y2, bins=np.logspace(0, np.log10(max(y1)), 100 ), alpha=.5, label='I' )
pl.gca().set_xscale("log")
pl.xlabel('CS number')
pl.ylabel('Numbers')
pl.legend(loc='upper right')
#pl.show()
pp.savefig()
pl.clf()

###
print "=== BlockIN/OUT =====  "
db.execute("""SELECT blockinput,blockoutput FROM `acct` where shtext>0""")
rows= db.fetchall()
yi=[]
yo=[]
for row in rows:
  yi.append(row[0])
  yo.append(row[1])

pl.hist(yi, bins=np.logspace( 1, 10, 100 ), alpha=.4,  label='In' )
pl.hist(yo, bins=np.logspace( 1, 10, 100 ), alpha=.4,  label='Out' )
pl.gca().set_xscale("log")
pl.xlabel('Size')
pl.ylabel('Numbers')
pl.legend(loc='upper right')
#pl.show()
pp.savefig()
pl.clf()


###
print "=== Run SHTXET =====  "
db.execute("""SELECT shtext FROM `acct` where shtext>0""")
rows= db.fetchall()
y=[]
for row in rows:
  y.append(row[0])

pl.hist(y, bins=np.logspace(np.log10(min(y)), np.log10(max(y)), 100 ),  label='Shtext' )
pl.gca().set_xscale("log")
pl.xlabel('Size')
pl.ylabel('Numbers')
pl.legend(loc='upper right')
#pl.show()
pp.savefig()
pl.clf()

###
print "=== User running successs job behavior =====  "
db.execute("""SELECT count(*) FROM `acct` group by uid""")
rows= db.fetchall()
ac=[]
for row in rows:
  ac.append(row[0])
db.execute("""SELECT count(*),sum(utime),sum(maxcpu*(logtime-gotime)) FROM `acct` where gotime!=0 and logtime-gotime>=0 and utime!=-1 group by uid""")
co=[]
ut=[]
su=[]
rows= db.fetchall()
for row in rows:
  co.append(row[0])
  ut.append(row[1])
  su.append(row[2])

pl.hist(co, bins=np.logspace(np.log10(min(ac)), np.log10(max(ac)), 200 ), alpha=.4, label='run' )
pl.hist(ac, bins=np.logspace(np.log10(min(ac)), np.log10(max(ac)), 200 ), alpha=.4, label='submit' )
pl.gca().set_xscale("log")
pl.xlabel('Jobs')
pl.ylabel('Number of Users')
pl.legend(loc='upper right')
#pl.show()
pp.savefig()
pl.clf()


pp.close()
stop
###########################################3








#n, bins, patches = plt.hist(co, 1000, normed=1, facecolor='green', alpha=0.75)
##y = mlab.normpdf( bins, mu, sigma)

#plt.plot(bins, 'r--', linewidth=1)
#plt.title(r'')
#plt.grid(True)
#plt.show()




###
print "=== CPU utiliztion and user =====  "
db.execute("""SELECT uid, count(*), sum(utime), sum(maxcpu*(logtime-gotime)) FROM `acct` where gotime!=0 and logtime-gotime>10 and utime!=-1  group by uid""")
rows= db.fetchall()
for row in rows:
  if row[1] > 500:
    print row[0], row[1], float(row[2])/float(row[3])

stop


###
print "=== CPU utiliztion > 0 =====  "
db.execute("""SELECT logtime,utime/(maxcpu*(logtime-gotime)), shtext FROM `acct` where gotime!=0 and logtime-gotime>300 and utime/(maxcpu*(logtime-gotime))>10 ORDER BY utime/(maxcpu*(logtime-gotime)) DESC""")
rows= db.fetchall()
#x = []
y = []
for row in rows:
  #x.append(row[0])
  y.append(row[1])
x = np.arange(len(y))
plt.plot(x, y, label="CPU eff vs time")
plt.xlabel('Time')
plt.legend(loc='upper right')
plt.show()
stop

###
print "=== CPU utiliztion > 0 =====  "
db.execute("""SELECT logtime,utime/(maxcpu*(logtime-gotime)) FROM `acct` where gotime!=0 and logtime-gotime>300 and utime/(maxcpu*(logtime-gotime))>10 ORDER BY utime/(maxcpu*(logtime-gotime)) DESC""")
rows= db.fetchall()
#x = []
y = []
for row in rows:
  #x.append(row[0])
  y.append(row[1])
x = np.arange(len(y))
plt.plot(x, y, label="CPU eff vs time")
plt.xlabel('Time')
plt.legend(loc='upper right')
plt.show()
stop

###
print "=== CPU utiliztion =====  "
db.execute("""SELECT logtime,utime/(maxcpu*(logtime-gotime)) FROM `acct` where gotime!=0 and logtime-gotime>300 and utime>0 ORDER BY utime/(maxcpu*(logtime-gotime)) DESC""")
rows= db.fetchall()
#x = []
y = []
for row in rows:
  #x.append(row[0])
  y.append(row[1])
x = np.arange(len(y))
plt.plot(x, y, label="CPU eff vs time")
plt.xlabel('Time')
plt.legend(loc='upper right')
plt.yscale('log', nonposy='clip')
plt.show()
stop

### 
print "=== CPU using behavior =====  "
db.execute("""SELECT maxcpu, count(*) as co FROM `acct` group by maxcpu ORDER by maxcpu""")
rows= db.fetchall()
x = []
y = []
for row in rows:
  x.append(row[0])
  y.append(row[1])
plt.plot(x, y, label="Jobs/CPU")
plt.xlabel('CPU')
plt.legend(loc='upper right')
plt.yscale('log', nonposy='clip')
plt.show()

###
print "=== Uncharged cour-hour !!! =====  "
##db.execute("""SELECT sum((maxcpu-nproc)*(logtime-gotime)/3600) FROM `acct` WHERE (nproc!=maxcpu and utime!=-1)""")
###print "Uncharged core.hours : %d " % db.fetchone()

db.execute("""SELECT uid, count(*), sum((maxcpu-nproc)*(logtime-gotime)/3600) as t FROM `acct` WHERE (nproc!=maxcpu and utime!=-1) group BY `uid` order by t ASC""")
rows= db.fetchall()
for row in rows:
 print row[0],"\t", row[1],"\t", round(row[2],2)


###
print "=== Utilization ====="
SUexp=25600*24*365*3.92
db.execute("""select sum(nproc*(logtime-gotime)/3600.) from acct where gotime!=0""" )
SUreal=float(db.fetchone()[0])
db.execute("""select sum(nproc*(logtime-gotime)/3600.) from acct where gotime!=0 and utime!= -1 """ )
SUgood=float(db.fetchone()[0])
print "25600 cores, Total expected core-hour in 3.92 yrs: %.2f" % SUexp
print "Total  run core-hour percentage: %.2f, ( %.2f )" % ( SUreal, SUreal/SUexp )
print "Total good core-hour percentage: %.2f, ( %.2f )" % ( SUgood, SUgood/SUreal)

###
db.execute("""select COUNT(*) from acct""")
alljobs=float(db.fetchone()[0])
print "Total job : %d " % alljobs
db.execute("""select COUNT(*) from acct where gotime = 0""" )
alljobs=float(db.fetchone()[0])
print "Job not run : %d " % db.fetchone()

###
print "=== System time ====="
db.execute("""select queue, count(queue), avg( stime/(nproc*(logtime-gotime))) as ss, max( stime/(nproc*(logtime-gotime)) ), min( stime/(nproc*(logtime-gotime)) )  from acct WHERE (gotime>0 and utime!=-1) GROUP BY queue ORDER BY ss;""" )
rows= db.fetchall()
for row in rows:
 print row[0], row[1], round(row[2],2)*100, " %", round(row[3],2), round(row[4],2)
