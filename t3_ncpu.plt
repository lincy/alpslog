#!/usr/bin/env python3
import re
import time
import datetime
#import MySQLdb
import tempfile
import slurm_util as slurm

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import mytools

matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 12})
matplotlib.rcParams.update({'lines.linewidth': 1})


data = np.loadtxt("t3_ncpu.dat")

plt.figure()
plt.plot(data[1:,0], data[1:,1])
plt.xlabel("Cores")
plt.ylabel("# Jobs")
plt.xscale('log')
plt.yscale('log')
plt.savefig("t3_ncpus.jpg")