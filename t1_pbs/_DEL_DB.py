import re
import time
import pymysql
import pymysql.cursors

def quote(s):
    return '"'+s+'"'

class PBSDB:

    HOST = 'localhost'
    USER = 'logana'
    PASS = 'password'

    def __init__(self, db):
        self._conn = pymysql.connect(host=self.HOST, user=self.USER, passwd=self.PASS, db=db, cursorclass=pymysql.cursors.DictCursor)
        self._cursor = self._conn.cursor()


    def exec(self, sql):
        self._cursor.execute(sql)
        res = self._cursor.fetchall()
        return res



    """
    state: other than PBS state, I use 'x' to indicate an unsettled log. To be [R]
    """
    def mk_tables(self):
        sql = '''
          CREATE TABLE IF NOT EXISTS acct (
          id INT UNIQUE NOT NULL AUTO_INCREMENT, jid CHAR(15), rcount SMALLINT UNSIGNED, user CHAR(20), proj CHAR(10), queue CHAR(15),
          qtime INT UNSIGNED DEFAULT 0, start INT UNSIGNED DEFAULT 0, end INT UNSIGNED DEFAULT 0,
          nhost SMALLINT UNSIGNED, ncpu SMALLINT UNSIGNED, ngpu SMALLINT UNSIGNED,
          cpup FLOAT UNSIGNED, cput FLOAT UNSIGNED, mem INT UNSIGNED, vmem INT UNSIGNED,
          ecode TINYINT, state CHAR(1), unixexit MEDIUMINT,
          PRIMARY KEY (start, jid) ) ENGINE=MyISAM DEFAULT CHARSET=latin1;
          '''
        self._cursor.execute(sql)
        self._conn.commit()
        sql = '''
          CREATE TABLE IF NOT EXISTS nodes ( id INT UNIQUE PRIMARY KEY, nodes TEXT ) ENGINE=MyISAM DEFAULT CHARSET=latin1;
          '''
        self._cursor.execute(sql)
        self._conn.commit()

    def drop_tables(self):
        self._cursor.execute("DROP TABLE acct;" )
        self._cursor.execute("DROP TABLE nodes;" )
        self._conn.commit()
        
    def desc_tables(self):
        sql = "DESC acct;"
        self._cursor.execute(sql)
        res = self._cursor.fetchall()
        print(res)
        sql = "DESC nodes;"
        self._cursor.execute(sql)
        res = self._cursor.fetchall()
        print(res)

    def _queueJob(self, jid,qtime):
        try:
            self._cursor.execute("INSERT INTO acct (jid, qtime, state) VALUES ({}, {}, {})"
                         .format(quote(jid), qtime, quote('Q')))
            self._conn.commit()
        except:
            self._cursor.execute("UPDATE acct SET qtime = {} WHERE jid = {} AND state={}".format(qtime, quote(jid), quote('Q')) )
            self._conn.commit()
    
    def _startJob(self, jid, start, qtime, user, group, proj, queue, node, ncpu):
        ##if (jid, start) exist, overwirte:
        try:
            self._cursor.execute("UPDATE acct SET \
              start={}, user={}, group={}, proj={}, queue={}, node={}, ncpu={}, state={}  \
              WHERE jid={} AND qtime={}"
              .format(start,quote(user),quote(group),quote(proj),quote(queue),node,ncpu,quote('S'),quote(jid), qtime ) )
            self._conn.commit()
        except pymysql.Error as e:
            self._cursor.execute("INSERT INTO acct \
              (jid, start, qtime, user, group, proj, queue, node, ncpu, state) VALUES \
              ({}, {}, {}, {}, {}, {}, {}, {}, {}, {}) "
              .format(jid, start,qtime,quote(user),quote(group),quote(proj),quote(queue),node,ncpu,quote('S')) )
            self._conn.commit()
        
    def _delJob(self, jid, logtime, state):
        assert state in 'QA', "state must be [QA]."
        try:
            self._cursor.execute("UPDATE acct SET (end={}, state={}) WHERE jid={}"   ### must del the last runcount
                .format(logtime, quote(state), quote(jid) ) )
            self._conn.commit()
        except:
            print("*** Dangaling job deleted.")


    def update_ngpu(self, jid, start, act, ngpu):
        assert act in 'ER', "state must be [QA]."
        try:
            self._conn.begin()
            self._cursor.execute("UPDATE acct SET ngpu={}  WHERE start={} AND jid={}"
              .format( ngpu, start,quote(jid)  ) )
            self._conn.commit()
        except Exception as e:
            raise(e)

    def endJob(self, jid, start, end, act, qtime, user, proj, queue, ecode, rcount, cpup, cput, mem, vmem, ncpu, ngpu, nodes, nhost):
        assert act in 'ER', "state must be [QA]."
        try:
            self._conn.begin()
            self._cursor.execute("INSERT INTO acct \
              (jid, start, end, state, qtime, user, proj, queue, ecode, rcount, cpup, cput, mem, vmem, ncpu, ngpu, nhost) VALUES \
              ({},  {},    {},  {},    {},  {},    {},  {},    {},    {},     {},   {},   {},  {},   {},   {}, {})"
              .format(quote(jid), start, end, quote(act), qtime, quote(user), quote(proj), quote(queue), ecode,rcount, cpup,cput,mem,vmem,ncpu,ngpu, nhost) )
            self._cursor.execute("INSERT INTO nodes (id, nodes) VALUES (LAST_INSERT_ID(), {})"
              .format( quote(nodes) ) )
            self._conn.commit()
        except Exception as e:
            print("*****")
            pass
