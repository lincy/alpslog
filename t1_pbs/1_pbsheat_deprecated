#!/usr/bin/env python3
import os
import re
import sys
import time
import datetime
import tempfile
import numpy as np

import pbs_util as pbs

T0 = 1525756594
T1 = 1651334388
TOTAL_SEC = T1-T0+1
UNIT = 600
NODEMAX = 1400
TAG="pbsmap"

NDIM = int(np.ceil(TOTAL_SEC/UNIT))
ts = np.arange(T0, T1, UNIT)

mint = T1*10
maxt = -1

class Counter:
    LINES    = 0
    JOBARRAY = 0
    CQTIME   = 0
    WTIME_MIS = 0
    WTIME_ZERO4ER = 0
    LOAD_LARGE  = 0
    LOAD_NORMAL = 0


def allFilesIn(root):
    fnames = []
    for root, dirs, files in os.walk(root):
        dirs.sort()
        for f in files:
            #print(os.path.join(root, f))
            fnames.append( os.path.join(root, f) )
    return fnames

allfiles = allFilesIn('/work/logana/T1_PBS_logs/T1_2021_job_logs')
allfiles = allFilesIn('/work/logana/T1_PBS_logs/T1_2018_job_logs')
allfiles = allFilesIn('/work/logana/T1_PBS_logs')
print("Total files: ", len(allfiles))



nfiles = 0
stop = False
npz = dict()
npz['E'] = np.zeros([NODEMAX, int(np.ceil(TOTAL_SEC/UNIT))]).astype(np.float32)
npz['R'] = np.zeros([NODEMAX, int(np.ceil(TOTAL_SEC/UNIT))]).astype(np.float32)
npz['A'] = np.zeros([NODEMAX, int(np.ceil(TOTAL_SEC/UNIT))]).astype(np.float32)


for fname in allfiles:
  if stop: break
  with open(fname, 'r') as f:
    for line in f:
      
      Counter.LINES+=1
      data = line.split(';')

      act     = data[1]
      jid     = data[2].split('.')[0]
    
      if jid.split('.')[0][-2:] == '[]':  ## Pass the master job array
        Counter.JOBARRAY +=1
        continue

      if act not in "ER": continue

      res = pbs.process_vals(data[3].strip())
        
      try:  
        cpup    = int(res['resources_used.cpupercent'])
        ct    = pbs.hms2secs( res['resources_used.cput']  )
        wt   = pbs.hms2secs( res['resources_used.walltime']  )
        mem     = int(res['resources_used.mem'][:-2])/1024  ## in MB
        vmem    = int(res['resources_used.vmem'][:-2])/1024  ## in MB
        nc    = int(res['resources_used.ncpus'])
        ncpu = int(res['Resource_List.ncpus'])
        node = int(res['Resource_List.nodect'])
        try:     ngpu = int(res['resource_assigned.ngpus'])
        except:  ngpu = 0
      except Exception as e:
        continue

      #user   = res['user']
      #proj  = res['project']
      queue = res['queue']
      start = int(res['start'])
      end   = int(res['end'])    ### should equal to logtime
      #ecode   = int(res['Exit_status'])
      #rcount  = int(res['run_count'])
      nodes   = pbs.parse_vnode(res['exec_vnode'])

      if (end > T1):
         print("WWWW: Early stopped...")
         stop = True
         break;

      eff_core = 0
      if wt > 0:
         eff_core = ct/wt
         if eff_core/nc > 1:
             #print("{}  Effcore/nc={}".format(jid, eff_core/nc))
             Counter.LOAD_LARGE +=1
         else:
             Counter.LOAD_NORMAL +=1
         

      for node in nodes:
        i0 = (start-T0)//UNIT
        i1 = (end-T0)//UNIT
        type = 'E'
        if act != 'E': type='R'
        npz[type][node, i0]      += eff_core/nc * (i0+1-(start-T0)/UNIT)
        npz[type][node, i0+1:i1] += eff_core/nc
        npz[type][node, i1]      += eff_core/nc * ((end-T0)/UNIT - i1)

        npz['A'][node, i0]      += ncpu/node * (i0+1-(start-T0)/UNIT)
        npz['A'][node, i0+1:i1] += ncpu/node
        npz['A'][node, i1]      += ncpu/node * ((end-T0)/UNIT - i1)

      if start < mint:          mint = start
      if end > maxt:            maxt = end

      if wt == 0:
          Counter.WTIME_ZERO4ER += 1

  nfiles+=1
  if nfiles%10 == 1: print ("Iter {} : processing {} ....".format(nfiles,fname) )
  #if nfiles > 10 : break
  print(Counter.__dict__)
    


if T0 > mint:
    print("WWWW: T0 too large:  min(start)-T0 = %d" % (start - T0) )

if mint > T0:
    print("IIII: T0 waste {} sec.".format(mint-T0) )
    #jE = jE[:, mint-T0:]
    #jR = jR[:, mint-T0:]
    #T0=mint

if T1 > maxt:
    print("IIII: T1 waste {} sec.".format(T1-maxt) )

print(Counter.__dict__)
MinT = datetime.datetime.fromtimestamp(mint).strftime('%Y-%m-%d %H:%M:%S')
MaxT = datetime.datetime.fromtimestamp(maxt).strftime('%Y-%m-%d %H:%M:%S')
print("Min time: {}  {}".format(mint, MinT))
print("Max time: {}  {}".format(maxt, MaxT))

np.savez_compressed("{}_{}_{}.npy".format(TAG, T0, UNIT), t=ts, **npz)
