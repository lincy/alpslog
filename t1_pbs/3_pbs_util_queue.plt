#!/usr/bin/env python3
import re
import time
import datetime
import tempfile

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import mytools

matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 12})
matplotlib.rcParams.update({'lines.linewidth': 1})


def plot_over_time(T0, T1, TAG, UNIT):
    INFN = "{}_{}.npy.npz".format(TAG, UNIT)

    ## Load data and remove the Unit Timestamp at first.
    data = dict(np.load(INFN))
    ts = data.pop('t')

    data.pop('ngs48G')
    data.pop('ngs96G')

    
    ## fit data to range
    fil = [ (t>=T0 and t<T1) for t in ts ]
    ts = ts[fil]
    for i in data:
        data[i] = data[i][fil]

    sorted_data = dict( sorted( data.items(), key=lambda x: np.sum(x[1]), reverse=True) )

    ## Sum over queue
    qsum = dict()
    for q in data:
        qsum[q] = np.sum(data[q])
    sum = np.sum(list(qsum.values()))
    

    ## History of # allocated cores per queue...
    midx = 0
    qperc_label = []
    for k in sorted_data.keys():
        if qsum[k]/sum < 0.0019:
            break
        midx += 1
        qperc_label.append("{} ({:3.1f}%)".format(k, qsum[k]/sum*100))

    print("Plot the first {} queue during {}-{} ...".format(midx, T0, T1))

    plt.figure(figsize=(16,9))
    plt.stackplot(ts, list(sorted_data.values())[:midx], labels=qperc_label )
    plt.xlabel("Twnia-1")
    plt.ylabel("Allocated Cores")
    taxis, tlabel = mytools.YYMM_axis(ts, 10)
    plt.xticks(taxis, tlabel)
    lg = plt.legend(loc='upper left', bbox_to_anchor=(1, 1), fontsize='xx-small', ncol=1, handleheight=1.8, labelspacing=0.1)

    plt.savefig("{}_{}_{}_{}.jpg".format(TAG,T0, T1, UNIT), bbox_extra_artists=(lg,), bbox_inches='tight')

#=============================
T0 =  1525756594
Tm = int(time.mktime(datetime.datetime.strptime("20210901", "%Y%m%d").timetuple()))
T1 =  1651334388
UNIT=3600*24
TAG="pbs_util_queue"
#=============================

plot_over_time(T0, T1, TAG, UNIT)
#plot_over_time(T0,   TF, TAG, UNIT)
#plot_over_time(T1+1, TF, TAG, UNIT)
