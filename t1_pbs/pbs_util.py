import re
import time
import pymysql
import pymysql.cursors

import time
import datetime

def sep_line(s):
    # 05/20/2018 00:01:28;D;20435.srvc1;requestor=cat83125@clogin1
    # don;t use split() in case if ';' appears in 'jobname' or elsewhere.
    res = [ s[:19], s[20:21] ]
    i = 22 + s[22:].find(';')
    res.append( s[22:i] )
    res.append( s[i+1:].strip() )
    return res

def process_vals(str):
    res = dict()
    for s in str.split(' '):
        d = s.split('=',1)
        try:
           res[d[0]] = d[1]
        except:
           pass
    return res;


def time2unix(str):
    ## 05/20/2018 00:09:29
    return datetime.datetime.timestamp( datetime.datetime.strptime(str, "%m/%d/%Y %H:%M:%S") )

def process_line(s):
    ## return time, jid, act, .....
    res = dict()
    res['t'] = time2unix( s[:19] )
    res['act']  = s[20:21]
    i = 22 + s[22:].find(';')
    res['jid'] = s[22:i]
    for kv in s[i+1:].strip().split():
        d = kv.split('=')
        try:     ## for example "D;33794.srvc1;Job deleted, execution node  down"
           res[d[0]] = d[1]
        except:
           pass
    return res


def hms2secs(str):
    d = [int (i) for i in str.split(':')]
    return 60*(60*d[0] + d[1] ) + d[2]
    
def parse_vnode(vnodes):  # to int
    #exec_vnode=(cn0961:ncpus=20)+(cn0963:ncpus=20)+(cn0963:ncpus=20)  or (cnbm01:ncpus=:ngpus=)
    nodes = dict()
    for n in vnodes.split('+'):
        d = n[1:-1].split(':')
        nid = 0
        if   d[0][2]=='b': nid=int(d[0][4:])
        else:              nid=int(d[0][2:])
        nodes[nid] = 1  ### TODO: consider also the ranks in a node...
    return list(nodes.keys())



#===========================================
def quote(s):
    return '"'+s+'"'

class PBSDB:

    HOST = 'localhost'
    USER = 'logana'
    PASS = 'password'

    def __init__(self, db = 'logana'):
        self._conn = pymysql.connect(host=self.HOST, user=self.USER, passwd=self.PASS, db=db, cursorclass=pymysql.cursors.SSDictCursor,local_infile=True)
        self._cursor = self._conn.cursor()

    def fetchall(self, sql):
        self._cursor.execute(sql)
        return self._cursor.fetchall()

    def execute(self, sql):
        self._cursor.execute(sql)

    def fetchone(self):
        return self._cursor.fetchone()

    def exec(self, sql):
        try:
            ret = self._cursor.execute(sql)
            self._conn.commit()
        except Exception as e:
            print("Exeception: {}".format(e))
        return ret

    ### just for test
    def close(self):
        self._conn.commit()
        self._cursor.close()
        self._conn.close()

    """
    state: other than PBS state, I use 'x' to indicate an unsettled log. To be [R]
    """
    def mk_tables(self, acct="acct", node="node" ):
        ## NOT NULL AUTO_INCREMENT
        sql1 = '''
          CREATE TABLE IF NOT EXISTS `{0}` (
          id INT UNIQUE NOT NULL AUTO_INCREMENT, jid CHAR(20), rcount TINYINT, user CHAR(20), proj CHAR(10), queue CHAR(20),
          qtime INT(11) UNSIGNED DEFAULT 0, start INT(11) UNSIGNED DEFAULT 0, end INT(11) UNSIGNED DEFAULT 0,
          nhost SMALLINT UNSIGNED, ncpu SMALLINT UNSIGNED, ngpu SMALLINT UNSIGNED,
          cpup FLOAT UNSIGNED, cput FLOAT UNSIGNED, mem FLOAT UNSIGNED, vmem FLOAT UNSIGNED,
          ecode SMALLINT, state CHAR(1),
          PRIMARY KEY (start, jid) ) ENGINE=MyISAM DEFAULT CHARSET=latin1;
          '''
        self._cursor.execute(sql1.format(acct))
        sql = "ALTER TABLE `{0}` ADD INDEX (start), ADD INDEX (end), ADD INDEX (jid), ADD INDEX (queue);"
        self._cursor.execute(sql.format(acct))
        sql2 = '''
          CREATE TABLE IF NOT EXISTS `{0}` ( id INT UNIQUE PRIMARY KEY, nodes TEXT ) ENGINE=MyISAM DEFAULT CHARSET=latin1;
          '''
        self._cursor.execute(sql2.format(node))
        self._conn.commit()

    def drop_tables(self, tn="tmp"):
        self._cursor.execute("DROP TABLE {};".format(tn) )
        self._conn.commit()
        
    def desc_tables(self, tn="acct"):
        sql = "DESC {};".format(tn)
        self._cursor.execute(sql)
        res = self._cursor.fetchall()
        print(res)

    def _queueJob(self, jid,qtime):
        try:
            self._cursor.execute("INSERT INTO acct (jid, qtime, state) VALUES ({}, {}, {})"
                         .format(quote(jid), qtime, quote('Q')))
            self._conn.commit()
        except:
            self._cursor.execute("UPDATE acct SET qtime = {} WHERE jid = {} AND state={}".format(qtime, quote(jid), quote('Q')) )
            self._conn.commit()
    
    def _startJob(self, jid, start, qtime, user, group, proj, queue, node, ncpu):
        ##if (jid, start) exist, overwirte:
        try:
            self._cursor.execute("UPDATE acct SET \
              start={}, user={}, group={}, proj={}, queue={}, node={}, ncpu={}, state={}  \
              WHERE jid={} AND qtime={}"
              .format(start,quote(user),quote(group),quote(proj),quote(queue),node,ncpu,quote('S'),quote(jid), qtime ) )
            self._conn.commit()
        except pymysql.Error as e:
            self._cursor.execute("INSERT INTO acct \
              (jid, start, qtime, user, group, proj, queue, node, ncpu, state) VALUES \
              ({}, {}, {}, {}, {}, {}, {}, {}, {}, {}) "
              .format(jid, start,qtime,quote(user),quote(group),quote(proj),quote(queue),node,ncpu,quote('S')) )
            self._conn.commit()
        
    def _delJob(self, jid, logtime, state):
        assert state in 'QA', "state must be [QA]."
        try:
            self._cursor.execute("UPDATE acct SET (end={}, state={}) WHERE jid={}"   ### must del the last runcount
                .format(logtime, quote(state), quote(jid) ) )
            self._conn.commit()
        except:
            print("*** Dangaling job deleted.")


    def update_ngpu(self, jid, start, act, ngpu):
        assert act in 'ER', "state must be [QA]."
        try:
            self._conn.begin()
            self._cursor.execute("UPDATE acct SET ngpu={}  WHERE start={} AND jid={}"
              .format( ngpu, start,quote(jid)  ) )
            self._conn.commit()
        except Exception as e:
            raise(e)
            

    def endJob(self, jid, start, end, act, qtime, user, proj, queue, ecode, rcount, cpup, cput, mem, vmem, ncpu, ngpu, nodes, nhost):
        assert act in 'ER', "state must be [QA]."
        try:
            self._conn.begin()
            self._cursor.execute("INSERT INTO acct \
              (jid, start, end, state, qtime, user, proj, queue, ecode, rcount, cpup, cput, mem, vmem, ncpu, ngpu, nhost) VALUES \
              ({},  {},    {},  {},    {},  {},    {},  {},    {},    {},     {},   {},   {},  {},   {},   {}, {})"
              .format(quote(jid), start, end, quote(act), qtime, quote(user), quote(proj), quote(queue), ecode,rcount, cpup,cput,mem,vmem,ncpu,ngpu, nhost) )
            self._cursor.execute("INSERT INTO nodes (id, nodes) VALUES (LAST_INSERT_ID(), {})"
              .format( quote(nodes) ) )
            self._conn.commit()
        except Exception as e:
            #print("*****")
            pass
