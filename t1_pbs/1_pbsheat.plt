#!/usr/bin/env python3
import os
import re
import sys
import time
import glob
import datetime
import pbs_util as pbs

import numpy as np
import matplotlib
import matplotlib.cm as cm
import matplotlib.pyplot as plt
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 60})
matplotlib.rcParams.update({'lines.linewidth': 4})

import mytools

TAG="pbsheat"

def getfiles(pattern):
  T0=[]
  UNIT=[]
  for f in glob.glob(pattern):
    t = f.split('.')[0].split('_')[1:3]
    T0.append(t[0])
    UNIT.append(t[1])
  return T0, UNIT
  

def plot_heat(ts, z, label, UNIT):   ### mem heat
      taxis, tlabel = mytools.YYMM_axis(ts, 10)
      zmin = np.min(z)
      zmax = np.max(z)
      print("Max: ", zmax)
      plt.figure(figsize=(95,40))
      cE = plt.imshow(z, cmap = mytools.cmBlues(), vmin = zmin, vmax = zmax, interpolation='none', origin ='lower',aspect='auto', alpha=1.0)
      plt.colorbar(cE)
      
      Ti0 = datetime.datetime.utcfromtimestamp(ts[0]).strftime('%Y-%m-%d %H:%M:%S')
      Ti1 = datetime.datetime.utcfromtimestamp(ts[-1]).strftime('%Y-%m-%d %H:%M:%S')
      plt.xlabel("Date: {} - {} ".format(Ti0, Ti1))
      plt.ylabel("Node")
      plt.xticks((taxis-ts[0])/UNIT, tlabel)
    
      plt.savefig("{}_{}_{}.jpg".format(label, T0,UNIT),  bbox_inches='tight' )

def plot_util_hist(d):
      matplotlib.rcParams.update({'font.size': 11})
      matplotlib.rcParams.update({'lines.linewidth': 1})

      plt.figure()
      vmin, vmax = np.min(d), np.max(d)
      bins = np.append(np.linspace(0,1,11), np.exp( np.linspace(np.log(2), np.log(vmax), 11 ))  )
      his, edge = np.histogram(d, bins)
      plt.plot(his)
      plt.xticks(range(len(his)), edge[:-1])
      
      plt.xlabel("CPU utilization per node")
      plt.ylabel("Density")
      plt.savefig("pbsheat_utilization_hist.jpg",  bbox_inches='tight' )


#T0s, UNITs = getfiles("{}_*.npz".format(TAG))
T0s   = [1525756594]
UNITs = [600 ]
T0s= [1640966400]

T0s   = [1525756594]
UNITs=[3600]    
UNITs=[86400]    
    
for T0, UNIT in zip(T0s, UNITs):
  INFN = "{}_{}_{}.npy.npz".format(TAG, T0, UNIT)

  T0   = int(T0)
  UNIT = int(UNIT)

  data = dict(np.load(INFN))
  ts = data.pop('t')
  d1 = data['E']
  d2 = data['R']
  dA = data['A']
  dm = data['mem']
  dv = data['vmem']
  block  = np.sum(d1+d2, axis=1)
  filter = block.astype(bool)


  print("Processing ... {} with shape {}".format(INFN, np.shape(d1[filter,:])))

  #plot_util_hist( d1[filter,:].flatten()  )

  #plot_heat(ts, d1[filter,:], TAG, UNIT)

  if 1:   ### load heat
    T = np.shape(ts)[0]
    NN = 1
    for i in range(NN):
      i0 = i     * (T//NN)
      i1 = (i+1) * (T//NN)

      taxis, tlabel = mytools.YYMM_axis(ts[i0:i1], 10)
      maxmat = 1.2  ##np.max(d1[:,i0:i1])
      print("Max: ", maxmat)
      plt.figure(figsize=(95,40))
      
      cE = plt.imshow(d1[filter,i0:i1], cmap = mytools.cmBlues(), vmin = 0, vmax = maxmat, interpolation='none', origin ='lower',aspect='auto', alpha=1.0)
      #cR = plt.imshow(d2[filter,i0:i1], cmap = mytools.cmReds(),  vmin = 0, vmax = maxmat, interpolation='none', origin ='lower',aspect='auto', alpha=0.4)
      #cE = plt.imshow(d1[filter,:], cmap = mytools.cmBlues(), vmin = 0, vmax = maxmat, interpolation='none', origin ='lower',aspect='auto')
      plt.colorbar(cE)
      
      Ti0 = datetime.datetime.utcfromtimestamp(ts[0]).strftime('%Y-%m-%d')
      Ti1 = datetime.datetime.utcfromtimestamp(ts[-1]).strftime('%Y-%m-%d')
      plt.xlabel("Date:  {} - {} ".format(Ti0, Ti1))
      plt.ylabel("Node")
      plt.xticks((taxis-ts[i0])/UNIT, tlabel)
    
      plt.savefig("{}_{}_{}_{}.jpg".format(TAG, T0,UNIT,i),  bbox_inches='tight' )
      #plt.savefig("pbsheat.jpg".format(TAG, T0,UNIT,i),  bbox_inches='tight' )

  
  #plot_heat(ts, data['mem'][filter,:],  "pbsmem", UNIT)
  #plot_heat(ts, data['vmem'][filter,:], "pbsvmem", UNIT)


  if 0:

    matplotlib.rcParams.update({'font.size': 11})
    matplotlib.rcParams.update({'lines.linewidth': 1})
    
    util1 = np.sum(d1, axis=0)
    util2 = np.sum(d2, axis=0)
    utilA = np.sum(dA, axis=0)

    taxis, tlabel = mytools.YYMM_axis(ts, 10)
    plt.figure(figsize=(13,5))
    plt.plot(ts, util1, 'b', label="Eff. cores (E)")
    plt.plot(ts, util2, 'r', label="Eff. cores (R)")
    #plt.plot(ts, utilA, 'k', label="Occupied cores")
    #plt.yscale("log")
    plt.xlabel("Date")
    plt.ylabel("Eff. Cores")
    plt.xticks(taxis, tlabel)
    plt.ylim(ymin=0.05)
    #plt.hlines(25200, ts[0], ts[-len(ts)//3], colors = 'g', linestyles ='dashed')
    plt.legend(frameon=False)
    plt.savefig("{}_su_1d.jpg".format(TAG),  bbox_inches='tight')

  if 0:

    matplotlib.rcParams.update({'font.size': 11})
    matplotlib.rcParams.update({'lines.linewidth': 1})
    
    utilm = np.sum(dm, axis=0)/1024
    utilv = np.sum(dv, axis=0)/1024

    taxis, tlabel = mytools.YYMM_axis(ts, 10)
    plt.figure(figsize=(13,5))
    plt.plot(ts, utilm, 'b', label="Mem / GB")
    plt.plot(ts, utilv, 'r', label="VMem / GB")
    #plt.yscale("log")
    plt.xlabel("Date")
    plt.ylabel("GByte")
    plt.xticks(taxis, tlabel)
    #plt.ylim(ymin=0.05)
    plt.legend(frameon=False)
    plt.savefig("{}_mem_1d.jpg".format(TAG),  bbox_inches='tight')



    
    

"""
try:
    print("{:15s} {:40s} {:5d} {:15.2f} {:7.2f}% {:15d}".format(jid, jname, nc, ct, ct/(etc)*100, et ) )
except:
    print(state)
    if state=="COMPLETED":
    print(jid, jname, nc, ct, totalcpu, etc, ct_str, et)
"""
