#!/usr/bin/env python3
import os
import re
import sys
import time
import datetime
import tempfile
import numpy as np
import pickle as pkl

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import mytools
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 12})
matplotlib.rcParams.update({'lines.linewidth': 1})

import pbs_util as pbs


db = pbs.PBSDB()

dt_sec = [60,3600,3600*24,3600*24*7,3600*24*30]
dt_lab = ['Min','Hour','Day','Week','Month']

###
FN = "t1_analysis.p"
res = db.exec('SELECT FLOOR(cput/(end-start)/ncpu*10) AS a, count(*) FROM acct where end-start > 0  group by a order BY a;')
try:
    with open( FN, "rb" ) as f:
       Nr = pkl.load(f)
       Twait= pkl.load(f)
       Uwait= pkl.load(f)
       Bwait= pkl.load(f)
       Trun= pkl.load(f)
       Urun= pkl.load(f)
       Brun= pkl.load(f)

except :
    Nr=dict()
    Twait=dict()
    Uwait=dict()
    Bwait=dict()
    Trun=dict()
    Urun=dict()
    Brun=dict()

    avg_wtime_run = db.exec("""select ncpu, count(ncpu), sum(start-qtime),max(start-qtime),min(start-qtime), \
                            sum(end-start),max(end-start),min(end-start) from acct WHERE (start>0) GROUP BY ncpu;""" )
    for item in avg_wtime_run:
      row = list(item.values())
      print (row)
      Nr[row[0]] = row[1]
      Twait[row[0]] = float(row[2])/row[1]
      Uwait[row[0]] = row[3]
      Bwait[row[0]] = row[4]
      Trun[row[0]] = float(row[5])/row[1]
      Urun[row[0]] = row[6]
      Brun[row[0]] = row[7]

      all = [ Nr, Twait, Uwait, Bwait, Trun, Urun, Brun ]
      with open( FN, "wb" ) as f:
         for i in all: pkl.dump(i, f)


xp = list(Nr.keys())
print(len(xp))

## Avg waiting======================================
fig, ax1 = plt.subplots(figsize=(10,4))
ax2 = ax1.twinx()

l1 = ax1.plot(xp, Twait.values(), 'b', marker='.', markersize=4, label="Avg waiting time")
ax1.fill_between(xp, list(Uwait.values()), list(Bwait.values()), facecolor='blue', alpha=0.2)
ax1.hlines(dt_sec, xp[0], xp[-len(xp)//3], colors = 'g', linestyles ='dashed')
for src,lab in zip(dt_sec, dt_lab): ax1.text(1.1, src, lab)
ax1.set_yscale('log')
ax1.set_ylim(ymin=1)
ax1.set_ylabel('Avg. waiting time (secs)')

l2 = ax2.plot(xp, Nr.values(), 'r', label="Num of jobs")
ax2.set_ylabel('Num of jobs')

# combine legend
lns = l1+l2
labs = [l.get_label() for l in lns]
ax1.legend(lns, labs, loc=0, frameon=False)

plt.xscale('log', base=2)
plt.xlim(xmin=1)
plt.gca().xaxis.set_major_formatter(FormatStrFormatter('%d'))
ax1.set_xlabel('ncores @ Twnia-1')
plt.savefig("t1_waittime.jpg",  bbox_inches='tight')

##  Avg run time ======================================

plt.figure(figsize=(10,4))
plt.plot(xp, Trun.values(), 'b', marker='.', markersize=4, label="Avg run time")
plt.fill_between(xp, list(Urun.values()), list(Brun.values()), facecolor='blue', alpha=0.2)
plt.hlines(dt_sec, xp[0], xp[-len(xp)//3], colors = 'g', linestyles ='dashed')
for src,lab in zip(dt_sec, dt_lab): plt.text(1.1, src, lab)
plt.xscale('log', base=2)
plt.yscale('log')
plt.gca().xaxis.set_major_formatter(FormatStrFormatter('%d'))
plt.xlim(xmin=1)
plt.ylim(ymin=1)
plt.xlabel('ncores @ Twnia-1')
plt.ylabel('Avg. run time (secs)')
plt.legend(loc='upper right', frameon=False)
plt.savefig("t1_runtime.jpg",  bbox_inches='tight')

###
#db.execute("""select queue, count(queue), sum(end-qtime),max(end-qtime),min(logtime-subtime) from acct WHERE (gotime=0) GROUP BY queue ORDER by queue;""" )
"""
avg_wtime = db.fetchall()
for row in avg_wtime:
  Nnrun[row[0]] = row[1]
  Tnrun[row[0]] = float(row[2])/row[1]/60
  Unrun[row[0]] = row[3]/60
  Bnrun[row[0]] = row[4]/60


xp = np.arange(len(Nnrun))
plt.xticks(xp, Nnrun.keys(),rotation=90)
plt.plot(xp, Tnrun.values(), label="Avg norun")
plt.plot(xp, Unrun.values(), label="Max norun")
plt.plot(xp, Bnrun.values(), label="Min norun")
plt.xlabel('Queue')
plt.legend(loc='upper right')
plt.show()
"""

###
#db.execute("""select queue, count(queue), sum(logtime-subtime),max(logtime-subtime),min(logtime-subtime) from acct WHERE ((exitInfo=14 or exitInfo=8 or exitInfo=10) and gotime=0) GROUP BY queue ORDER BY queue;""" )
"""
avg_wtime_user_kill = db.fetchall()
for row in avg_wtime_user_kill:
  Nkbrun[row[0]] = row[1]
  Tkbrun[row[0]] = row[2]/row[1]/60
  Ukbrun[row[0]] = row[3]/60
  Bkbrun[row[0]] = row[4]/60

xp = np.arange(len(Nkbrun))
plt.xticks(xp, Nkbrun.keys(),rotation=90)
plt.plot(xp, Tkbrun.values(), label="Avg kbrun")
plt.plot(xp, Ukbrun.values(), label="Max kbrun")
plt.plot(xp, Bkbrun.values(), label="Min kbrun")
plt.xlabel('Queue')
plt.legend(loc='upper right')
plt.show()
"""

