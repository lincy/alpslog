#!/usr/bin/env python3
import re
import time
import datetime
import slurm_util as slurm
import mytools

import numpy as np
import matplotlib
import matplotlib.cm as cm
import matplotlib.pyplot as plt
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 70})
matplotlib.rcParams.update({'lines.linewidth': 4})

#======================
UNIT=300
TAG="t3_heatmap"
#======================

INFN = "{}_{}.npy.npz".format(TAG,UNIT)

#with open(LOAD_MAT, 'rb') as f:
data = dict(np.load(INFN))
ts = data.pop('t')

d1 = data['ld']
d2 = data['fail']
d3 = data['occupy']
df = data['bfill']

## fit data to range
#fil = [ (t>=T0 and t<T1) for t in ts ]
#ts = ts[fil]
#for i in data:
#    data[i] = data[i][fil]

### y-axis
node_filter  = np.sum(d1,axis=1).astype(bool)
#nodeaxis = np.arange( load.shape[0] ) + 3000
#nodeaxis = nodeaxis[node_filter]
#naxis  = [0,200,400,600,800,1000]
#nlabel = [ str(n) for n in naxis ]


###  Plot 1000 x 24
if __name__ == "__main__":
    
  print("D1 range: {} {}".format( np.min(d1), np.max(d1) ))
  print("D2 range: {} {}".format( np.min(d2), np.max(d2) ))
  taxis, tlabel = mytools.YYMM_axis(ts, 10)

  if 1:

    maxval = 1.2 #np.max(load)
    plt.figure(figsize=(100,40))
    cE = plt.imshow(d1[node_filter,:]+d2[node_filter,:], cmap = mytools.cmBlues(), norm=None, vmax=maxval, interpolation='none', origin ='lower',aspect='auto', alpha=1.0)
    cR = plt.imshow(df[node_filter,:], cmap = mytools.cmReds(),  norm=None, vmax=maxval, interpolation='none', origin ='lower',aspect='auto', alpha=0.5)
    plt.colorbar(cE)
    plt.xticks((taxis-ts[0])/UNIT, tlabel)
    plt.ylabel("Node index")
    #plt.yticks(naxis,       nlabel)
    plt.savefig("{}_{}.jpg".format(TAG,UNIT),  bbox_inches='tight')

  if 0:

    maxval = np.max(d3)
    print("Max occupied node: ", maxval)
    maxval = 58
    plt.figure(figsize=(100,40))
    c = plt.imshow( d3[node_filter,:], cmap = mytools.cmBlues(), vmax=maxval, interpolation='none', origin ='lower',aspect='auto', alpha=1.0)
    plt.colorbar(c)
    plt.xticks((taxis-ts[0])/UNIT, tlabel)
    plt.ylabel("Node index")
    #plt.yticks(naxis,       nlabel)
    plt.savefig("{}_{}_occupy.jpg".format(TAG,UNIT),  bbox_inches='tight')


  if 0:
    matplotlib.use('Agg')
    matplotlib.rcParams.update({'font.size': 12})
    matplotlib.rcParams.update({'lines.linewidth': 1})

    util1 = np.sum(d1,axis=0)
    util2 = np.sum(d2,axis=0)
    util3 = np.sum(d3,axis=0)
    
    plt.figure(figsize=(10,4))
    plt.plot(ts, util1, label="Completed jobs")
    plt.plot(ts, util2, label="Others")
    plt.plot(ts, util3, label="Occupy")
    plt.yscale("log")
    plt.ylabel("Cores")
    plt.ylim(ymin=1)
    plt.xticks(taxis, tlabel)
    plt.legend(frameon=False)
    plt.savefig("{}_{}_1d.jpg".format(TAG,UNIT),  bbox_inches='tight')


