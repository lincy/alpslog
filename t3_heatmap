#!/usr/bin/env python3
import re
import time
import datetime
import tempfile
import slurm_util as slurm

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 10})
matplotlib.rcParams.update({'lines.linewidth': 1})

#============================
T0 = 1617011537
T1 = 1655112467
TOTAL_SEC = T1-T0+1
UNIT=300
TAG="t3_heatmap"
NODEMAX=2100
#============================

iU=1.0/UNIT

NDIM = int(np.ceil(TOTAL_SEC/UNIT))
ts = np.arange(T0, T1, UNIT)

T3LOG = '/work/logana/T3-slurm-acct-all.txt'
T3LOG = '/work/logana/t3_selected.log'

L =   slurm.get_label_dic(T3LOG)                 ## get labels string from raw log
NC = len(L)
#print(L)


T0h = datetime.datetime.utcfromtimestamp(T0).strftime('%Y-%m-%d %H:%M:%S')
T1h = datetime.datetime.utcfromtimestamp(T1).strftime('%Y-%m-%d %H:%M:%S')
print("Processing {} ....".format(T3LOG))
print("Process {} sec from {} to {} ..".format(TOTAL_SEC, T0h, T1h))

mint = 999999999999
maxt = -1

count = 0
npz = dict()
npz['ld'] = np.zeros([NODEMAX, int(np.ceil(TOTAL_SEC/UNIT))]).astype(np.float32)
npz['fail'] = np.zeros([NODEMAX, int(np.ceil(TOTAL_SEC/UNIT))]).astype(np.float32)
npz['occupy'] = np.zeros([NODEMAX, int(np.ceil(TOTAL_SEC/UNIT))]).astype(np.float32)
npz['bfill'] = np.zeros([NODEMAX, int(np.ceil(TOTAL_SEC/UNIT))]).astype(np.float32)
with open(T3LOG, 'r') as f:
    f.readline()
    for line in f:

        data = slurm.process_line(line)
        count+=1
        if count%200000==0: print("Processing ..", count)

        #if len(data) != NC+1:  print(data)
        
        if '.' in data[L["JobID"]]:    continue

        try:
          jid = data[L["JobID"]]
          jname = data[L["JobName"]]
          nn  = int(data[L["NNodes"]])
          nc  = int(data[L["NCPUS"]])
          wt  = int(data[L["ElapsedRaw"]])
          flag  = data[L["Flags"]]
          #ut  = slurm.str2secs( data[L["UserCPU"]] )
          #st  = slurm.str2secs( data[L["SystemCPU"]] )
          tt  = slurm.str2secs( data[L["TotalCPU"]] )
          #csec = int(data[L["CPUTimeRAW"]])
          
          state = data[L["State"]]
          #rss = data[L["MaxRSS"]]

          #tsub  = slurm.time2unix( data[ L["Submit"] ] )
          start  = slurm.time2unix( data[ L["Start"] ] )
          end  = slurm.time2unix( data[ L["End"] ] )
          nodes = slurm.get_nodelist(data[ L["NodeList"] ])
        except Exception as e:
          print("Length : ", len(data), line)
          raise(e)

        if start == 0 or end == 0:
            #print(" *** START or END is 0: ", jid, queue, ct, load)
            continue
        elif start > end:
            raise "WWWW: Job end before start..."

        if start < mint:          mint = start
        if end > maxt:            maxt = end

        if (end > T1):
            print("WWWW: Early stopped...")
            break;

        ##################
        if nn != len(nodes) and wt > 0:   ##ly happend when FAIL or CANCL
           print(wt, nn, nodes, jid, state, data[ L["NodeList"]])
           raise(0)

        util = 0
        if wt>0:
           util  = tt/wt/nc
           if util > 5:  print("{} Effcore/nc={} nn={} nc={} wt={} ct={}, state={}".format(jid,util,nn,nc,
                                wt,tt, state ) )

        #print(jid, nc, ct, load)

        i0 = (start-T0)//UNIT
        i1 = (end-T0)//UNIT
        r0 = (start-T0)*iU
        r1 = (end-T0)*iU
        for node in nodes:
            type = 'fail'
            if state.startswith("COMPLETED") or state.startswith("CANCELLED"): type = 'ld'
            npz[type][node-3000, i0]      += util * (i0+1-r0)
            npz[type][node-3000, i0+1:i1] += util
            npz[type][node-3000, i1]      += util * (r1 - i1)

            npz['occupy'][node-3000, i0]      += nc * (i0+1-r0)
            npz['occupy'][node-3000, i0+1:i1] += nc
            npz['occupy'][node-3000, i1]      += nc * (r1 - i1)

            if not flag.startswith("SchedMain"):
               npz['bfill'][node-3000, i0]      += util * (i0+1-r0)
               npz['bfill'][node-3000, i0+1:i1] += util
               npz['bfill'][node-3000, i1]      += util * (r1 - i1)
        #if count>400000: break

if T0 > mint:
    print("WWWW: T0 too large:  min(start)-T0 = %d" % (start - T0) )
if mint > T0:
    print("IIII: T0 waste {} sec.".format(mint-T0) )
    #jE = jE[:, mint-T0:]
    #jR = jR[:, mint-T0:]
    #T0=mint

if T1 > maxt:
    print("IIII: T1 waste {} sec.".format(T1-maxt) )

MinT = datetime.datetime.utcfromtimestamp(mint).strftime('%Y-%m-%d %H:%M:%S')
MaxT = datetime.datetime.utcfromtimestamp(maxt).strftime('%Y-%m-%d %H:%M:%S')
print("Min time: {}  {}".format(mint, MinT))
print("Max time: {}  {}".format(maxt, MaxT))

np.savez_compressed("{}_{}.npy".format(TAG, UNIT), t=ts, **npz)

node_filter  = np.sum(npz['ld'],axis=1).astype(bool)
print("Max nodes:", np.sum(node_filter) )

