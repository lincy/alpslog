import datetime
import numpy as np
import matplotlib
import matplotlib.cm as cm
import matplotlib.pyplot as plt

from matplotlib.colors import ListedColormap, LinearSegmentedColormap

def cmReds():
    N=101
    xr = np.linspace(1,0,N)
    new = np.zeros((N,4))
    new[:,0] = 1
    new[-1,0] = 0.5
    new[:,1] = xr[:]
    new[:,2] = xr[:]
    new[:,3] = 1
    return ListedColormap(new)

def cmBlues():
    N=101
    xr = np.linspace(1,0,N)
    new = np.zeros((N,4))
    new[:,0] = xr[:]
    new[:,1] = xr[:]
    new[:,2] = 1
    new[-1,2] = 0.5
    new[:,3] = 1
    return ListedColormap(new)

def YYMM_axis(x, num):
    """
    Generate <num> YYYY-MM axis for <x> array starting from <T0> with <UNIT> sec.
    """
    N = len(x)
    dx = int(N / (num - 1))
    positions = x[::dx]
    labels    = [ datetime.datetime.fromtimestamp(i).strftime('%Y-%m') for i in positions ]
    return positions, labels

