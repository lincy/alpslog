#!/usr/bin/env python
import re
import time
import datetime

DELIMIT='|'
SLURM_NODELIST_RE = re.compile(r'([cgb]??(\d+|\[[\d\,\-]+\]),?)')
##bgmXXXX, [cg]pnXXXX


"""
SELECTED_LABELS=" \
Account,AllocCPUS,AllocGRES,AllocNodes,AllocTRES,AssocID,AveCPU,AveCPUFreq,AveDiskRead,AveDiskWrite,AvePages,AveRSS,AveVMSize, \
CPUTime,CPUTimeRAW,DerivedExitCode,ElapsedRaw,Eligible,End,ExitCode,Flags,JobID,JobIDRaw, \
MaxDiskRead,MaxDiskWrite,MaxPages,MaxRSS,MaxVMSize, \
NNodes,NodeList,NTasks,\
Priority,Partition,ReqCPUS,ReqGRES,ReqMem,ReqNodes,ReqTRES,Reserved,ResvCPURAW,\
Start,State,Submit,Suspended,SystemCPU,TimelimitRaw,TotalCPU,\
UID,User,UserCPU\
".split(',')
"""

def process_line(s, delim=DELIMIT):
    res = s.split(delim)
    return res
    
def get_label_dic(fname, delim=DELIMIT):
    with open(fname, 'r') as f:
        labels = f.readline().split(delim)[:-1]
        return dict( zip(labels, range(len(labels)) ) )

def time2unix(s):
    # 2022-06-13T23:31:42
    try:
        return int( time.mktime( datetime.datetime.strptime(s, "%Y-%m-%dT%H:%M:%S").timetuple() ) )
    except:
        return 0

def str2secs(str):
    ## |1-01:18:31| or |00:00.71|
    try:
        d   =  [0]+str.split('-')
        tmp = d[-1].split('.')+[0]
        h   = ([0]+tmp[0].split(':'))[-3:]
        return int(d[-2])*86400 + int(h[0])*3600+int(h[1])*60+int(h[2])+0.001*int(tmp[1])
    except:
        return -1.0




#def label2idx(sel, label_dict):
#    return [ label_dict[i.strip()] for i in sel ]


"""
Parse nodelist string to integer list
"""
def expand_idx(idx):
    # assume index parts are same
    if idx.startswith('['):
        idx = idx[1:-1]
    idx_parts = idx.split(',')
    indexes = []
    for part in idx_parts:
        if "-" in part:
            start, stop = part.split('-', 2)
            indexes.extend(range(int(start), int(stop)+1))
        else:
            indexes.append(int(part))
    
    indexes.sort()
    return indexes

def expand_nodestr(idx_list):
    return [idx for idx in idx_list]

def get_nodelist(str):
    res = []
    for match in SLURM_NODELIST_RE.finditer(str):
        #print (match.groups())
        idx = expand_idx(match.group(2))
        if match.group(1)[0] == 'g':
           idx = [i + 1000 for i in idx]
        elif match.group(1)[0] == 'b':
           idx = [i + 2000 for i in idx]
        res += idx
    return res
